﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
    class Program
    {
        static void Main(string[] args)
        {
            var collection=new HusbandCollection2();

         


            foreach (var husband in collection )
            {
                Console.WriteLine(husband.Name);
                if (husband.Name=="Marian") break;
                ;
            }
            foreach (var husband in collection.items)
            {
                Console.WriteLine(husband.Name);
                Console.WriteLine(husband.Cos);
            }

            //collection.Reset();
            var ss=collection.Where(p => p.Name.StartsWith("S")).ToList();
            Console.ReadKey();
        }
    }

    


    class HusbandCollectionEnumerator : IEnumerator
    {
        public bool MoveNext()
        {
            throw new NotImplementedException();
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }

        public object Current { get; }
    }

    class HusbandCollection2 :  IEnumerable<Husband>
    {
        public Husband[] items =
        {
            new Husband("Stefan"),
            new Husband("Marian"),
            new Husband("Aleksander"),
            new Husband("Wiesław"),
        };

        public IEnumerable<Husband> GetList()
        {
            foreach (var husband in items)
            {
                husband.Cos = 5;
            }

            return items.ToList();
        }
        public IEnumerator<Husband> GetEnumerator()
        {
            foreach (var husband in items)
            {
                husband.Cos = 6;
                yield return husband;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
    class HusbandCollection : IEnumerable<Husband>, IEnumerator<Husband>
    {
        private Husband[] items =
        {
            new Husband("Stefan"),
            new Husband("Marian"),
            new Husband("Aleksander"),
            new Husband("Wiesław"),
        };

        public IEnumerator<Husband> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            
        }

        private int index = -1;
        public bool MoveNext()
        {
            
            if (index < items.Length-1)
            {
                index++;
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public void Reset()
        {
            index = -1;
        }

        public Husband Current => items[index];

        object IEnumerator.Current => Current;
    }

    class Husband
    {
        
        public Husband(string name)
        {
            Name = name;
        }


        public string Name { get; set; }
        public int Cos { get; set; }
    }
}
