﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Producent p1= new Producent("Amazon");
            Producent p2 = new Producent("Google");
            Producent p3 = new Producent("Codementors");
            int[] req = new int[] {2, 5, 6, 7, 8, 12, 5, 6, 7,};
            p1.SetSuccessor(p2);
            p2.SetSuccessor(p3);
            p1.Handle(req.ToList());

            Console.ReadKey();
        }

    }

    class Producent
    {
        public string Name { get; }
        public int Resources { get; set; }
        public Producent(string name)
        {
            Name = name;
            Resources = 22;
        }
        public void SetSuccessor(Producent p2)
        {
            this.NextProducent = p2;
        }

        public Producent NextProducent { get; set; }

        public void Handle(List<int> req)
        {
            for (var index = 0; index < req.Count; index++)
            {
                var r = req[index];
                if (Resources > r)
                {
                    Console.WriteLine($"jestem{Name} i produkuje za{r}");
                    
                    req.RemoveAt(index);
                    Resources -= r;
                    index--;

                }
                else
                {
                    NextProducent?.Handle(req);
                    break;
                }
            }
        }
    }
}
