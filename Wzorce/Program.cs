﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wzorce
{
    class Program
    {
        static void Main(string[] args)
        {
            Person wife = new Person("Karyna");
            Person husband = new Person("Eustachy");
            Person kid = new Person("Brajan");
            Person exHusband = new Person("Sebastian");


            Mediator mediator =new Mediator();

            mediator.Register(wife);
            mediator.Register(husband);
            mediator.Register(kid);

            wife.Send("Oto papiery rozwodowe", husband);
            wife.Send("Odchodze");
            Console.ReadKey();
        }
    }

    abstract class  Document
    {
        public void Print()
        {
            Console.WriteLine(Page1());
            Console.WriteLine(Page2());
        }

        protected abstract string Page1();
        protected abstract string Page2();
    }

    class Resume:Document
    {
        protected override string Page1()
        {

            return "ResumePage1";
        }

        protected override string Page2()
        {
            return "ResumePage2";
        }
    }



    class Mediator
    {
        private List<IPerson> clients = new List<IPerson>();
        public void Register(IPerson person)
        {
            clients.Add(person);
            person.Mediator = this;
        }

        public void Send(string message, IPerson destination)
        {
        

            var dest=clients.FirstOrDefault(p => p == destination);
            Console.WriteLine($"Mediator wysyla do:{dest?.Name} : {message}");
            dest?.Receive(message);            
        }

        public void SendAll(IPerson sender,string message)
        {
            Console.WriteLine($"Mediator wysyla do wszyscy : {message}");

            foreach (var client in clients)
            {
                if (client.Name!=sender.Name)
                    client.Receive(message);
            }
        }
    }

     interface IPerson
    {
        void Send(string message, IPerson destination);
        void Send(string message);
  
        Mediator Mediator { get; set; }
        string Name { get; set; }

        void Receive(string message);
    }
    class Person:IPerson
    {
        public string Name { get; set; }

        public Person(string name)
        {
            Name = name;
            

        }

        public void Send(string message, IPerson destination)
        {
            Mediator.Send(message, destination);
        }

        public void Send(string message)
        {
            Mediator.SendAll(this,message);
        }

        public Mediator Mediator { get; set; }
        public void Receive(string message)
        {
            Console.WriteLine($"Jestem {Name} dostalem:{message}");
        }

    }
    
}
