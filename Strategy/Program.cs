﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>()
            {
                new Employee(){Name = "John", Salary = 5000},
                new Employee(){Name = "Mirek", Salary = 12000},
                new Employee(){Name = "Alfred", Salary = 3000},
                new Employee(){Name = "Adam", Salary = 4000},



            };
            PrintAll(employees);

            SortAlgoritm<Employee> sortAlgoritm = new SortAlgoritm<Employee>();
            sortAlgoritm.CompareAlgorithm = new NameCompareAlgorithm();
            var rr = sortAlgoritm.Sort(employees);
            sortAlgoritm.CompareAlgorithm = new MoneyCompareAlgorithm();
            PrintAll(rr);
            var rr2 = sortAlgoritm.Sort(employees);
            PrintAll(rr2);

            Console.ReadKey();
        }

        private static void PrintAll(List<Employee> rr)
        {
            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            foreach (var employee in rr)
            {
                Console.WriteLine($"{employee.Name} - {employee.Salary}");
            }
        }
    }

    internal class MoneyCompareAlgorithm : CompareAlg<Employee>
    {
        public int Compare(Employee a, Employee b)
        {
            return a.Salary.CompareTo(b.Salary);
        }
    }

    internal class NameCompareAlgorithm : CompareAlg<Employee>
    {
        public int Compare(Employee a, Employee b)
        {
            return a.Name.CompareTo(b.Name);
        }
    }

    class SortAlgoritm<T>
    {
        public CompareAlg<T> CompareAlgorithm { get; set; }
        public List<T> Sort(List<T> list)
        {
            List<T> sortedList=list.ToList();
            for (int i = 0; i < sortedList.Count; i++)
            {
                for (int j = 0; j < sortedList.Count; j++)
                {
                    if (CompareAlgorithm.Compare(sortedList[i], sortedList[j])<0)
                    {
                        T temp = sortedList[i];
                        sortedList[i] = sortedList[j];
                        sortedList[j] = temp;
                        
                    }
                }
            }

            return sortedList;

        }
    }

    interface CompareAlg<T>
    {
        int Compare(T a, T b);

    }

    public class Employee
    {
        public string Name  { get; set; }
        public int Salary { get; set; }
    }

}
