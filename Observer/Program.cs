﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            Owner owner = new Owner();
            Cat cat = new Cat();

            cat.Owner = owner;

            cat.IsIll = true;
            cat.IsIll = false;
            cat.PropertyChanged += Cat_PropertyChanged;
            cat.Name = "kotek";
            Console.ReadKey();
        }

        private static void Cat_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Console.WriteLine($"Zmienila sie{e.PropertyName}");
        }
    }

    class Owner
    {
        public void CatIsIll(bool value)
        {
            if (value)
            {
                Console.WriteLine("Moj Kot jest chory");
            }
            else
            {
                Console.WriteLine("Moj Kot nie jest chory");

            }
        }
    }

    class Cat:INotifyPropertyChanged
    {
        private bool _isIll;
        private string _name;

        public bool IsIll
        {
            get => _isIll;
            set
            {
                _isIll = value;
                Owner.CatIsIll(value);
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public Owner Owner { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }


}
