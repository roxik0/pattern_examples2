﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor
{
    class Program
    {
        static void Main(string[] args)
        {
            City wladyslawowo = new City("Wlad") {NextCity = null};
            City gdynia=new City("Gdy"){NextCity = wladyslawowo};
            City sopot = new City("Sop"){NextCity = gdynia};
            City gdansk= new City("Gda"){NextCity = sopot};

            Taxi taxiDriver=new Taxi(){CurrentCity = gdansk};

            taxiDriver.MoveNextCity();
            taxiDriver.MoveNextCity();
            taxiDriver.MoveNextCity();
            taxiDriver.MoveNextCity();
            taxiDriver.MoveNextCity();
            taxiDriver.MoveNextCity();

            Console.ReadKey();

        }



    }

    public class Taxi
    {
        public City CurrentCity  { get; set; }

        public void MoveNextCity()
        {
            if (CurrentCity.Name == "Wlad")
            {
                Console.WriteLine("Jestem u Celu");
                return;
            }
            if (CurrentCity.Name == "Sop")
            {
                
                
            }

            if (CurrentCity.Name == "Gdy")
            {
                CurrentCity.GrazynaIsHere = true;
            }

            CurrentCity = CurrentCity.NextCity;
        }
    }
    public class City
    {
       
        public City(string name)
        {
            this.Name = name;
        }

        public City NextCity { get; set; }
        public string Name { get; set; }
        public bool GrazynaIsHere { get; set; }
    }


}
