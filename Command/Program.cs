﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Command
{
    class Program
    {
        static void Main(string[] args)
        {
            var wife=new Person("Grażyna");
            var husband = new Person("Janusz");

            var post = new Post();
            post.SendMessage(wife, husband, "Odchodze");
            post.SendMessage(wife,husband,"Podzielmy sie majątkiem");
            husband.HandleMessages(post.GetMessages(husband));
            post.SendMessage(husband,wife,"Spoko zarabiasz wiecej");

            Account acc=new Account();
            acc.ChangeBalance(+1000);
            acc.ChangeBalance(-400);
            acc.ChangeBalance(-400);
            acc.ChangeBalance(+1000);

            Console.WriteLine( acc.CurrentBalance());
            acc.changes.RemoveAt(2);
            Console.WriteLine(acc.CurrentBalance());

            Console.ReadKey();


        }
        
    }

    class Account
    {
        public List<AccountChange> changes=new List<AccountChange>();

        public void ChangeBalance(decimal balance)
        {
            changes.Add(new AccountChange(){balanceChange=balance});
        }

        public decimal CurrentBalance()
        {
            decimal balance = 0;
            foreach (var accountChange in changes)
            {
                balance += accountChange.balanceChange;
            }

            return balance;
        } 

    }

    internal class AccountChange
    {
        public decimal balanceChange { get; set; }
    }

    class Message
    {
        public Person Sender { get; set; }
        public Person Receiver { get; set; }
        public string Content { get; set; }
    }

    internal class Post
    {
        readonly List<Message> messages=new List<Message>();
        public void SendMessage(Person sender, Person receiver, string message)
        {
            messages.Add(new Message()
            {
                Sender = sender,
                Receiver = receiver,
                Content = message               
            });
        }

        public IEnumerable<Message> GetMessages(Person receiver)
        {
            return messages.Where(msg => msg.Receiver == receiver);
        }
    }

    class Person
    {
        private readonly string _name;

        public Person(string name)
        {
            _name = name;
        }

        public void HandleMessage(Message msg)
        {
            Console.WriteLine($"Dostalem wiadomosc od:{msg.Sender._name} o tresci: {msg.Content}");
        }

        public void HandleMessages(IEnumerable<Message> messages)
        {
            foreach (var msg in messages)
            {
                HandleMessage(msg);
            }
            
        }
    }
}
