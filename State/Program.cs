﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    class Program
    {
        static void Main(string[] args)
        {
            Account acc = new Account();
            
            acc.Deposit(1000);
            acc.Withdraw(400);
            acc.EndOfMonth();
            acc.Deposit(1000);
            acc.Withdraw(400);
            acc.EndOfMonth();

            Console.ReadKey();
        }
        
    }

    public abstract class AccountState
    {
        public decimal MonthlyInterest { get; set; }
    }

    public class NormalAccountState : AccountState
    {
        public NormalAccountState()
        {
            MonthlyInterest = 0.01m;
        }
    }

    public class PremiumAccount : AccountState
    {
        public PremiumAccount()
        {
            MonthlyInterest = 0.011m;
        }
    }

    public class Account
    {
        public decimal Balance { get; set; }
        public AccountState AccountState { get; set; } = new NormalAccountState();
        public void Deposit(decimal amount)
        {
            Balance += amount;
            Console.WriteLine($"Deposit EOM:Current Balance:{Balance}");
            CheckState();
        }

        private void CheckState()
        {
            if (Balance > 1000) { AccountState=new PremiumAccount(); }
            else
            {
                AccountState=new NormalAccountState();
            }
        }

        public void Withdraw(decimal amount)
        {
            Balance -= amount;
            Console.WriteLine($"Withdraw EOM:Current Balance:{Balance}");

        }
        public void EndOfMonth()
        {
            Console.WriteLine($"#########################");
            Console.WriteLine($"State:{AccountState.GetType().Name}");
            Console.WriteLine($"Before EOM:Current Balance:{Balance}");
            Balance = Balance + Balance * AccountState.MonthlyInterest;
            Console.WriteLine($"EOM:Current Balance:{Balance}");
            Console.WriteLine($"#########################");
        }
    }
}
