﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class Program
    {
        static void Main(string[] args)
        {
            string expression= "5 + 2 * 2 * 3";
            int result = new MathInterpreter().Calculate(expression);
            Console.WriteLine(result);
            Console.ReadKey();
        }
    }

    interface IExpression
    {
        int Calculate();
    }

    class NumberExpression:IExpression
    {
        public int Number { get; set; }
        public int Calculate()
        {
            return Number;
        }
    }

    internal class MathInterpreter
    {
        public int Calculate(string expressionStr)
        {
            string[] str = expressionStr.Split(' ');

            List<IExpression> expressions=new List<IExpression>();
            foreach (var s in str)
            {
                if (s.All(char.IsDigit))
                {
                    expressions.Add(new NumberExpression(){Number = int.Parse(s)});
                }

                if (s == "+")
                {
                    expressions.Add(new AddExpression());
                }

                if (s == "*")
                {
                    expressions.Add(new MultiplyExpression());
                }
            }

            while (expressions.Count > 1)
            {
                //Important Expression
                InterprateMultiply(expressions);
                InterprateAdd(expressions);
            }

            return expressions.First().Calculate();

        }

        private static void InterprateAdd(List<IExpression> expressions)
        {
            for (var i = 0; i < expressions.Count; i++)
            {
                var expression = expressions[i];
                if (expression is AddExpression)
                {
                    var mltExp = expression as AddExpression;
                    mltExp.A = expressions[i - 1];
                    mltExp.B = expressions[i + 1];

                    expressions.RemoveAt(i - 1);
                    expressions.RemoveAt(i);
                    i--;

                }
            }
        }

        private static void InterprateMultiply(List<IExpression> expressions)
        {
            for (var i = 0; i < expressions.Count; i++)
            {
                var expression = expressions[i];
                if (expression is MultiplyExpression)
                {
                    var mltExp = expression as MultiplyExpression;
                    mltExp.A = expressions[i - 1];
                    mltExp.B = expressions[i + 1];

                    expressions.RemoveAt(i - 1);
                    expressions.RemoveAt(i );
                    i--;

                }
            }
        }
    }

    internal class MultiplyExpression : IExpression
    {
        public IExpression A { get; set; }
        public IExpression B { get; set; }
        public int Calculate()
        {
            return A.Calculate() * B.Calculate();
        }
    }

    internal class AddExpression : IExpression
    {
        public IExpression A { get; set; }
        public IExpression B { get; set; }

        public int Calculate()
        {
            return A.Calculate() + B.Calculate();
        }
    }
}
